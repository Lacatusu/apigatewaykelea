'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  email: {
    type: String,
    required:true
  },
  from: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true
  },
  data: {
    displayName:{
      type: String,
      required: true
    },
    photoUrl:{
      type: String,
      required:true
    }
  }
});

module.exports = mongoose.model('users', UserSchema);
