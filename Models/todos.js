'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const TodoSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  notes: {
    type: String,
    required: false
  },
  startDate: {
    type: Date,
    required: false,
    default: null
  },
  dueDate: {
    type: Date,
    required: false,
    default: null
  },
  completed: {
    type: Boolean,
    required:true,
    default: false
  },
  starred: {
    type: Boolean,
    required:true,
    default: false
  },
  important: {
    type: Boolean,
    required:true,
    default: false
  },
  deleted: {
    type: Boolean,
    required:true,
    default: false
  },
  labels: {
    type: Array,
    required:true
  }
});

module.exports = mongoose.model('todos', TodoSchema);
