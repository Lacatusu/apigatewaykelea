'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const FiltersSchema = new Schema({
  handle: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  icon: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('filters', FiltersSchema);
