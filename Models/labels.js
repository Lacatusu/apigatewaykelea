'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const LabelsSchema = new Schema({
  handle: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  color: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('labels', LabelsSchema);
