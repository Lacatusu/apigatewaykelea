'use strict';

const express  = require('express');
const users    = require('../../lib/Users');
const get    = require('../../lib/Todos');
const router   = express.Router();
const passport  = require('passport');

/*----------------------------------------USERS OPTIONS---*/
//Get Options
router.get('/api/todo-app/todos', get.getTodos);
router.get('/api/todo-app/labels',get.getLabels);
router.get('/api/todo-app/filters',get.getFilters);
router.get('/api/todo-app/folders',get.getFolders);

//Post Options
router.post('/register', users.registerUsers);
router.post('/login', users.loginUsers);
router.post('/api/todo-app/new-todo',get.registerTodos);
router.post('/api/todo-app/update-todo',get.updateTodo);
router.post('/api/todo-app/remove-todo',get.deleteTodo);

//DELETE Options

module.exports = router;
