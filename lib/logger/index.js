/*eslint-disable max-len */
/*eslint-disable no-shadow */

'use strict';

const {createLogger, format, transports} = require('winston');
const chalk = require('chalk').default;
const {combine, colorize, label, printf, splat, timestamp} = format;

const logFormat = (loggerLabel)=> combine(
    timestamp(),
    splat(),
    colorize(),
    label({label: loggerLabel}),
    printf((info)=> `[${info.timestamp}] ${chalk.cyan(info.label)} ${info.level}: ${info.message}`)
);

const createLoggerWithLabel = (label)=> createLogger({
    format: logFormat(label),
    level: 'info',
    transports: [new transports.Console({})]
});

module.exports = {
    info: createLoggerWithLabel('API')
};
