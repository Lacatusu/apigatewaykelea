'use strict';

const Todo    = require('../../Models/todos');
const Label   = require('../../Models/labels');
const Filters = require('../../Models/filters');
const Folders = require('../../Models/folders');

const getTodos = (req,res)=> {
    const key   = Object.keys(req.query)[0];
    const value = Object.values(req.query)[0];

    if (key === 'labelHandle') {
        Label.find().then((labels)=> {
            const labelID = labels.find((label)=> label.handle === value)._id.toString();

            Todo.find().then((todos)=> {
                const resp = todos.filter((todo)=> todo.labels.includes(labelID) && !todo.deleted);
                res.json(resp);
            });
        });

    } else if (key === 'filterHandle') {
        Todo.find().then((todos)=> {
            const resp = todos.filter((todo)=> todo[value] && !todo.deleted);
            res.json(resp);
        });
    } else {
        let folderHandler = key;
        if (!folderHandler) {
            folderHandler = 'all';
        }
        if (folderHandler === 'all') {
            Todo.find().then((todos)=> {
                const resp = todos.filter((todo)=> !todo.deleted);
                res.json(resp);
            });
        } else {
            Todo.find().then((todos)=> {
                res.json(todos);
            });
        }
    }
};

const getLabels = (req,res,next)=> {
    Label.find().
        populate('labels').
        then((labels)=> {
            res.json(labels);
        }).
        catch((err)=> res.status(404).json({labels: 'There are no profiles'}));
};

const getFilters = (req,res,next)=> {
    Filters.find().
        populate('filters').
        then((filters)=> {
            res.json(filters);
        }).
        catch((err)=> res.status(404).json({filters: 'There are no profiles'}));
};

const getFolders = (req,res,next)=> {
    Folders.find().
        populate('folders').
        then((folders)=> {
            res.json(folders);
        }).
        catch((err)=> res.status(404).json({folders: 'There are no profiles'}));
};

const registerTodos         =  (req, res)=> {
    Todo.find().then((user)=> {

        const newTodo = new Todo({
            title: req.body.title,
            notes: req.body.notes,
            startDate: req.body.startDate,
            dueDate: req.body.dueDate,
            completed: req.body.completed,
            starred: req.body.starred,
            important: req.body.important,
            deleted: req.body.deleted,
            labels: req.body.labels
        });

        newTodo.
            save().
            then((todo)=> res.json(todo)).
            catch((err)=> console.log(err));
    });
};

const updateTodo = (req,res,next)=> {

    Todo.findOne({_id: req.body._id}).then((todo)=> {
        Todo.findOneAndUpdate(
            {_id: req.body._id},
            {$set: req.body},
            {new: true}
        ).then((todo)=> res.json(todo));

    });
};

const deleteTodo = (req,res,next)=> {

    Todo.findOneAndRemove({_id: req.body._id}).then(()=> {
        res.json({success: true});
    });
};

module.exports = {
    getTodos,
    getLabels,
    getFilters,
    getFolders,
    registerTodos,
    updateTodo,
    deleteTodo
};
