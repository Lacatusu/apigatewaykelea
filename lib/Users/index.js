'use strict';

const bcrypt                = require('bcryptjs');
const User                  = require('../../Models/users');
const validateRegisterInput = require('../Validator/register');
const validateLoginInput    = require('../Validator/login');
const jwt                   = require('jsonwebtoken');

const registerUsers         =  (req, res)=> {
const { errors, isValid }   = validateRegisterInput(req.body);

  // Check Validation
  /*if (!isValid) {
    return res.status(400).json(errors);
  }*/

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      errors.email = 'Email already exists';
      return res.status(400).json(errors);
    } else {
      const newUser = new User({
        from     : req.body.from,
        password : req.body.password,
        role     : req.body.role,
        email    : req.body.email,
        data     : req.body.data
      });

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => res.json(err));
        });
      });
    }
  });
}

const loginUsers = (req, res)=> {
  const { errors, isValid } = validateLoginInput(req.body);
  
  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find user by email
  User.findOne({ email }).then(user => {
    // Check for user
    if (!user) {
      errors.email = 'User not found';
      return res.status(404).json(errors);
    }

    // Check Password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User Matched
        const payload = { id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        cifIdentification: user.cifIdentification,
        clientType:user.clientType }; // Create JWT Payload
        const secret  = "secretOrKey";
        // Sign Token
        const jwtBearerToken = jwt.sign(
          payload,
          secret,
          { expiresIn: 3600 },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer ' + token
            });
          }
        );        
      } else {
        errors.password = 'Password incorrect';
        return res.status(400).json(errors);
      }
    });
  });
}

module.exports = {
  registerUsers,
  loginUsers
}