'use strict';

//External dependencies
const bodyParser = require('body-parser');
const express    = require('express');
const mongoose   = require('mongoose');
const cors       = require('cors');
const passport   = require('passport');

//Internal controllers
const db         = require('./config/mongo').mongoURI;
const users      = require('./Routes/Users');
const logger     = require('./lib/logger').info;
//Declare express init to initialize the app...
const app        = express();

//Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(cors());

//MongoDB connection
mongoose.
    connect(db, {useNewUrlParser: true}).
    then(()=> logger.info('MongoDB Connected')).
    catch((err)=> logger.error(err));

//Passport Config
app.use(passport.initialize());
require('./config/security/passport')(passport);

//Using routes
app.use('/api/v2/', users);

//Init server
require('dotenv').config();
const port = process.env.PORT;

app.listen(port, ()=> {
    logger.info(`Node server running in port: ${port}`);
});
